import { combineReducers } from "redux";
import drinkReducer from "./pizza.reducer";

const rootReducer = combineReducers({
    drinkReducer
 });
 
 export default rootReducer;
import { DRINK_FETCH_ERROR, DRINK_FETCH_PENDING, DRINK_FETCH_SUCCESS } from "../constants/pizza.constant";

const initialState = {
    drinks: [],
    pending: false
};

const drinkReducer = (state = initialState, action) => {
    switch (action.type) {
        case DRINK_FETCH_PENDING:
            state.pending = true;
            break;
        case DRINK_FETCH_SUCCESS:
            state.pending = false;
            state.drinks = action.data;
            break;
        case DRINK_FETCH_ERROR:
            break;
        default:
            break;
    }

    return {...state};
}

export default drinkReducer;
export const DRINK_FETCH_PENDING = "Trạng thái đợi khi gọi API lấy danh sách drink";

export const  DRINK_FETCH_SUCCESS = "Trạng thái thành công khi gọi API lấy danh sách drink";

export const  DRINK_FETCH_ERROR = "Trạng thái lỗi khi gọi API lấy danh sách drink";
import { DRINK_FETCH_ERROR, DRINK_FETCH_PENDING, DRINK_FETCH_SUCCESS } from "../constants/pizza.constant";

export const fetchDrink = () => {
    return async(dispatch) => {
        try {
            var requestOptions = {
                method: "GET"
            }

            await dispatch({
                type: DRINK_FETCH_PENDING
            });

            const resDrinks = await fetch("http://203.171.20.210:8080/devcamp-pizza365/drinks", requestOptions);

            const dataDrinks = await resDrinks.json();

            return dispatch({
                type: DRINK_FETCH_SUCCESS,
                data: dataDrinks
            });
        } catch (error) {
            return dispatch({
                type: DRINK_FETCH_ERROR,
                error: error
            });
        }
    }
}
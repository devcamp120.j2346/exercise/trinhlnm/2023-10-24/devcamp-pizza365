import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';

class FormComponent extends Component {
    render() {
        return <>
            <div id="contact" className="row">
                <div className="col-sm-12 text-center p-4 mt-4">
                    <h2><b className="p-2 border-bottom">Thông tin đơn hàng</b></h2>
                </div>

                <div className="col-sm-12 p-2"
                    style={{ backgroundColor: "#f8f7f7", borderRadius: "10px" }}
                >
                    <div className="row">
                        <div className="col-sm-12 p-4">
                            <div className="form-group mb-2">
                                <label for="input-name">Họ và tên</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="input-name"
                                    placeholder="Họ và tên"
                                />
                            </div>
                            <div className="form-group mb-2">
                                <label for="email">Email</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="input-email"
                                    placeholder="Email"
                                />
                            </div>
                            <div className="form-group mb-2">
                                <label for="input-phone">Điện thoại</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="input-phone"
                                    placeholder="Điện thoại"
                                />
                            </div>
                            <div class="form-group mb-2">
                                <label for="input-address">Địa chỉ</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="input-address"
                                    placeholder="Địa chỉ"
                                />
                            </div>
                            <div class="form-group mb-2">
                                <label for="message">Lời nhắn</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="input-message"
                                    placeholder="Lời nhắn"
                                />
                            </div>

                            <div className="form-group mb-4">
                                <label for="message">Mã giảm giá (Voucher ID)</label>
                                <input
                                    type="text"
                                    className="form-control"
                                    id="input-voucherID"
                                    placeholder="Mã voucher"
                                />
                            </div>

                            <button
                                type="button"
                                className="btn-green"
                            >
                                Kiểm tra đơn
                            </button>
                        </div>
                    </div>
                </div>

                <div
                    id="div-container-order"
                    className="container bg-info p-2"
                    style={{ display: "none", backgroundColor: "#f8f7f7" }}
                >
                    <div id="div-order-info" className="text-white p-3">
                        thông tin đơn hàng vào đây
                    </div>
                    <div className="p-2">
                        <button
                            type="button"
                            className="btn-yellow"
                        >
                            Gửi đơn
                        </button>
                    </div>
                </div>

                <div
                    className="container bg-warning p-3 mt-2"
                    style={{ display: "none", backgroundColor: "#f8f7f7" }}
                    id="div-container-Cam-on"
                >
                    <div>
                        Cảm ơn bạn đã đặt hàng tại Pizza 365. Mã đơn hàng của bạn là:
                    </div>
                    <div style={{ display: "flex", alignItems: "center" }}>
                        <div>Mã đơn hàng:</div>
                        <div
                            id="div-ma-don-hang"
                            style={{
                                width: "50%",
                                border: "1px black solid",
                                margin: "1rem",
                                paddingTop: "3px",
                                paddingLeft: "1rem"
                            }}
                        ></div>
                    </div>
                </div>
            </div>
        </>;
    }
}

export default FormComponent;
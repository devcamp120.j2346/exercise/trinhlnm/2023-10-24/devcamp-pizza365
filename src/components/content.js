import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';
import IntroduceComponent from "./introduceComponent";
import SizeComponent from "./sizeComponent";
import TypeComponent from "./typeComponent";
import DrinkComponent from "./drinkComponent";
import FormComponent from "./formComponent ";

class Content extends Component {
    render() {
        return <>
            <div class="container" style={{ padding: "70px 0px 50px 0px" }}>
                <div class="row">
                    <div class="col-sm-12">
                        <IntroduceComponent/>
                        <SizeComponent/>
                        <TypeComponent/>
                        <DrinkComponent/>
                        <FormComponent/>
                    </div>
                </div>
            </div>
        </>;
    }
}

export default Content;
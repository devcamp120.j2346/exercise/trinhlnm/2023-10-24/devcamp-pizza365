import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';

class IntroduceComponent extends Component {
    render() {
        return <>
            <div class="row">
                <div class="col-sm-12">
                    <h1><b class="text-main">Pizza 365</b></h1>
                    <p style={{ fontStyle: "italic" }} class="text-second">
                        Truly italian!
                    </p>
                </div>
                <img alt='' src={require("../assets/images/anh_mon_1.jpg")} />

                <div class="col-sm-12 text-center p-4 mt-4">
                    <h2><b class="p-2 border-bottom">Tại sao lại Pizza 365</b></h2>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-3 p-3 bg-brand-01 text-white border">
                            <h3 class="p-2">Không ngừng cải tiến</h3>
                            <p class="p-2">
                                Cải tiến sản phẩm, chất lượng và trải nghiệm khách hàng luôn
                                là yếu tố tiên quyết trong chiến lược kinh doanh của chúng
                                tôi nhằm thúc đẩy tăng trưởng và trở thành thương hiệu pizza
                                hàng đầu.
                            </p>
                        </div>
                        <div class="col-sm-3 p-3 bg-brand-02 text-white border">
                            <h3 class="p-2">Trải nghiệm dễ dàng</h3>
                            <p class="p-2">
                                Chúng tôi đơn giản hướng đến việc trở thành thương hiệu của
                                mọi nhà, mọi lúc, mọi nơi.
                            </p>
                        </div>
                        <div class="col-sm-3 p-3 bg-brand-03 text-white border">
                            <h3 class="p-2">Hướng đến sự vượt trội</h3>
                            <p class="p-2">
                                "Vượt trội" không chỉ là một khái niệm lớn. Thay vào đó, giá
                                trị này là cốt lõi trong mỗi con người của chúng tôi, trong
                                mỗi công việc mà họ làm.
                            </p>
                        </div>
                        <div class="col-sm-3 p-3 bg-brand-04 text-white border">
                            <h3 class="p-2">Thể hiện đam mê</h3>
                            <p class="p-2">
                                Chúng tôi sẵn lòng hỗ trợ vô điều kiện và linh động điều
                                chỉnh tùy theo thử thách để tìm ra giải pháp.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </>;
    }
}

export default IntroduceComponent;
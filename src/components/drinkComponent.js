import { useEffect } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';
import { useDispatch, useSelector } from "react-redux";
import { fetchDrink } from "../actions/drink.action";

const DrinkComponent = () => {
    const dispatch = useDispatch();

    const { drinks, pending } = useSelector(reduxData => reduxData.drinkReducer);

    useEffect(() => {
        dispatch(fetchDrink());
    }, []);

    return <>
        <div style={{ marginTop: "3rem" }}>
            <label
                for="select-drink"
                style={{ fontWeight: "600", fontSize: "2rem", textAlign: "center", width: "100%" }}
            >Menu Đồ Uống</label>
            <br />
            <select id="select-drink" style={{ width: "100%", padding: "0.5rem" }}>
                <option value="0">-- Chọn đồ uống --</option>
                {
                    pending ? <></>
                        : drinks.map((e) => {
                            return <option value={e.maNuocUong}>{e.tenNuocUong}</option>
                        })
                }
            </select>
        </div>
    </>;
}

export default DrinkComponent;
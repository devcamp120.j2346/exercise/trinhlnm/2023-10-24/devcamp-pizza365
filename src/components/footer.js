import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';
import { FaArrowUp } from "react-icons/fa";
import { FaSnapchat } from "react-icons/fa";
import { FaPinterestP } from "react-icons/fa";
import { AiFillFacebook } from "react-icons/ai";
import { AiOutlineInstagram } from "react-icons/ai";
import { AiOutlineTwitter } from "react-icons/ai";
import { BiLogoLinkedin } from "react-icons/bi";

class Footer extends Component {
    render() {
        return <>
            <div className="container-fluid bg-light p-3">
                <div className="row text-center">
                    <div className="col-sm-12">
                        <h4 className="m-2">Footer</h4>
                        <div className="btn text-white m-3" style={{ backgroundColor: "#00628f" }}
                        ><FaArrowUp style={{ marginTop: "-2px" }} />   To the top</div>
                        <div className="m-2">
                            <AiFillFacebook className='text-main' />
                            <AiOutlineInstagram className='text-main' />
                            <FaSnapchat className='text-main' />
                            <FaPinterestP className='text-main' />
                            <AiOutlineTwitter className='text-main' />
                            <BiLogoLinkedin className='text-main' />
                        </div>
                    </div>
                </div>
            </div>
        </>;
    }
}

export default Footer;
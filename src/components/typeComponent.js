import { Component } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import '../App.css';

class TypeComponent extends Component {
    render() {
        return <>
            <div className="row">
                <div className="col-sm-12 text-center p-4 mt-4">
                    <h2><b className="p-2 border-bottom">Chọn loại pizza</b></h2>
                </div>
                <div className="col-sm-12">
                    <div className="row">
                        <div className="col-sm-4">
                            <div className="card w-100" style={{ width: "18rem" }}>
                                <img
                                    src={require("../assets/images/pizza_type_hawai.jpg")}
                                    class="card-img-top"
                                    alt=''
                                />
                                <div className="card-body">
                                    <h3>Pizza Hawai</h3>
                                    <p>Món ăn thanh đạm</p>
                                    <p>
                                        Hãy thưởng thức món ăn với phong cách Alo Ha đến từ
                                        Hawai.
                                    </p>
                                    <p>
                                        <button
                                            class="btn-green"
                                            id="btn-type-hawai"
                                        >
                                            Chọn
                                        </button>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div className="col-sm-4">
                            <div className="card w-100" style={{ width: "18rem" }}>
                                <img
                                    src={require("../assets/images/pizza_type_seafood.jpg")}
                                    class="card-img-top"
                                    alt=''
                                />
                                <div className="card-body">
                                    <h3>Pizza Hải sản</h3>
                                    <p>Món ăn đến từ biển</p>
                                    <p>
                                        Bạn đã thử pizza được chế biến từ nguyên liệu hải sản
                                        chưa ?
                                    </p>
                                    <p>
                                        <button
                                            class="btn-green"
                                            id="btn-type-hawai"
                                        >
                                            Chọn
                                        </button>
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div className="col-sm-4">
                            <div className="card w-100" style={{ width: "18rem" }}>
                                <img
                                    src={require("../assets/images/pizza_type_bacon.jpg")}
                                    class="card-img-top"
                                    alt=''
                                />
                                <div className="card-body">
                                    <h3>Pizza Bacon</h3>
                                    <p>Món ăn đăc biệt</p>
                                    <p>
                                        Được chế biến từ thịt bacon. Mang đến hương vị mới lạ.
                                    </p>
                                    <p>
                                        <button
                                            class="btn-green"
                                            id="btn-type-hawai"
                                        >
                                            Chọn
                                        </button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>;
    }
}

export default TypeComponent;